(function() {
	'use strict';
	angular
		.module('icebergMovies')
		.controller('MainController', MainController);

	MainController.$inject = ['$scope', '$log', 'MovieService'];

	function MainController($scope, $log, MovieService) {
		var vm = this;

		vm.searchOptions = {
			pageNumber: 1,
			pageSize: 20,
			start: 1,
			end: 20,
			sortBy: 'title',
			searchTerm: null
		};

		vm.searchHint = '';

		vm.getTimes = function(n) {
			return new Array(n);
		}

		vm.getResultPage = function(pageNum) {
			if(pageNum <= vm.movies.totalPages) {
				vm.searchOptions.pageNumber = pageNum;
				vm.getResults();
			}

			return false;
		}

		vm.getActorList = function(actors) {
			var actorList = [];

			angular.forEach(actors, function(value, key){
				actorList.push(value.name);
			})
			return actorList.join();
		}

		vm.getResults = function() {
			MovieService.getList(vm.searchOptions).then(function(data){
				vm.movies = data;

				if(vm.movies.length) {
					vm.searchOptions.start = (vm.movies.matchedItems / vm.movies.pageNumber);
				}
				else {
					vm.searchHint = 'No matching items';
				}
			})
		}

		$scope.$watch('searchString', function(searchString) {
			vm.searchHint = '';
			vm.searchOptions.searchTerm = null;

			if(searchString && searchString.length > 0 && searchString.length < 3) {
				vm.searchHint = 'Enter at least three characters to begin search';
			}
			else if(searchString && searchString.length >= 3) {
				vm.searchOptions.searchTerm = searchString;
				vm.getResults();
			}
			//else if(!searchString) {
			//	vm.getResults();
			//}
		});

		activate();

		function activate() {
			vm.getResults();
		}

	}
})();
