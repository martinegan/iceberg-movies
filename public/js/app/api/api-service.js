'use strict';
angular
  .module('APIService', ['restangular'])
  .config(function(RestangularProvider) {
    RestangularProvider.setBaseUrl('/api');
});

angular
  .module('APIService')
  .factory('ResourceWithPaging', ResourceWithPaging);

  ResourceWithPaging.$inject = ['Restangular'];

  function ResourceWithPaging(Restangular) {
    var resource;
		resource = Restangular.withConfig(function(RestangularProvider) {
			return RestangularProvider.setResponseExtractor(function(response, operation) {
				var newResponse, ref, ref1, ref2, ref3, ref4, ref5, ref6;

				if (operation === 'get') {
					//console.log(response);

					newResponse = (ref = response.content) != null ? ref : [];
					newResponse.pageNumber = (ref1 = response.pageNumber) != null ? ref1 : 1;
					newResponse.matchedItems = (ref2 = response.matchedItems) != null ? ref2 : 0;
					newResponse.totalItems = (ref3 = response.totalItems) != null ? ref3 : 0;
					newResponse.totalPages = (ref4 = response.totalPages) != null ? ref4 : 0;
					newResponse.startItem = (ref5 = response.startItem) != null ? ref5 : 0;
					newResponse.endItem = (ref6 = response.endItem) != null ? ref6 : 0;

					return newResponse;
				} else {
					return response;
				}
			});
		});
		return resource;
  }
