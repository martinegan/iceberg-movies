module.exports = function (config) {

    config.set({

        // list of files / patterns to load in the browser
        files: [
            'bower_components/angular/angular.min.js',
            'bower_components/angular-mocks/angular-mocks.js',
            'bower_components/angular-route/angular-route.js',
            'bower_components/restangular/dist/restangular.min.js',
            'bower_components/lodash/lodash.min.js',
            'bower_components/jquery/dist/jquery.min.js',
            'bower_components/bootstrap/dist/js/bootstrap.min.js',

            'public/js/**/*.js',
            'test/unit/**/*.spec.js'
        ],

        preprocessors: {
            'public/js/**/*.js': 'coverage'
        },

        // list of files to exclude
        exclude: [],


        proxies: {},

        // web server port
        port: 9876,

        // enable / disable colors in the output (reporters and logs)
        colors: true,
        autoWatch: true,
        usePolling: true,

        // frameworks to use
        frameworks: ['jasmine', 'sinon'],
        browsers : ['PhantomJS'],

        customLaunchers: {
            ChromeDesktop: {
                base: 'Chrome',
                flags: ['--window-size=1280,720']
            }
        },

        plugins: [
            'karma-jasmine',
            'karma-sinon',
            'karma-chrome-launcher',
            'karma-mocha-reporter',
            'karma-phantomjs-launcher',
            'karma-coverage'
        ],

        // Test results reporter to use
        // possible values: 'dots', 'progress', 'mocha', 'junit', 'growl', 'coverage'
        reporters: ['mocha', 'coverage', 'progress'],

        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: true

    });
};



