var hasClass = function (element, cls) {
    return element.getAttribute('class').then(function (classes) {
        return classes.split(' ').indexOf(cls) !== -1;
    });
};

describe('Iceberg Movies', function(){

    describe('Home Page', function(){
        describe('Navigation Bar', function(){
            it('should exist', function() {
                browser.get('/');
                expect(element(by.css('.navbar')).isPresent()).toBe(true);
            });
        })

        describe('Search Input Box', function(){
            it('div.searchbox should exist', function(){
                browser.get('/');
                expect(element(by.css('div.search-box')).isPresent()).toBe(true);
                expect(element(by.css('.search-input')).isPresent()).toBe(true);
            })

            it('search input should exist', function(){
                browser.get('/');
                expect(element(by.css('.search-input')).isPresent()).toBe(true);
            })

            it('entering less than 3 characters should show hint message', function(){
                browser.get('/');
                element(by.id('searchString')).sendKeys("a");

                browser.wait(function() {
                    return browser.isElementPresent(by.css('#messagebox'));
                }, 5000);

                expect(element(by.css('#messagebox')).getText()).toBe('Enter at least three characters to begin search');
            })

            it('entering 3 or more characters should perform search and limit results', function(){
                browser.get('/');
                element(by.id('searchString')).sendKeys("val");

                browser.wait(function() {
                    return browser.isElementPresent(by.css('.search-results'));
                }, 5000);

                expect(element(by.css('.search-summary')).isPresent()).toBe(true);
                expect(element(by.css('.search-summary')).getText()).toBe('Showing 1 to 2 of 2 movies found');

                expect(hasClass(element(by.css('#messagebox')), 'ng-hide')).toBe(true);

                element.all(by.css('.thumbnail')).then(function(items) {
                    expect(items.length).toBe(2);;
                });
            })
        })

        describe('Search Results', function(){
            it('should load correctly', function(){
                browser.get('/');

                browser.wait(function() {
                    return browser.isElementPresent(by.css('.search-results'));
                }, 5000);

                expect(element(by.css('.search-summary')).isPresent()).toBe(true);
                expect(element(by.css('.search-summary')).getText()).toBe('Showing 1 to 20 of 160 movies found');

                element.all(by.css('.thumbnail')).then(function(items) {
                    expect(items.length).toBe(20);;
                });


            })

            it('should contain the pager', function(){
                browser.get('/');

                browser.wait(function() {
                    return browser.isElementPresent(by.css('.pager'));
                }, 5000);

                expect(element(by.css('.pager-summary')).isPresent()).toBe(true);
                expect(element(by.css('.pager-summary')).getText()).toBe("Displaying page 1 of 8");

                expect(element(by.css('.pager-previous')).isPresent()).toBe(true);
                expect(element(by.css('.pager-next')).isPresent()).toBe(true)

                element.all(by.css('.pager-item')).then(function(items) {
                    expect(items.length).toBe(8);;
                });
            })
        })

        describe('Performing Search', function(){
            it('entering a search term which returns no results should show "No matching items', function(){
                browser.get('/');
                element(by.id('searchString')).sendKeys("aaa");

                browser.wait(function() {
                    return browser.isElementPresent(by.css('#messagebox'));
                }, 5000);

                expect(element(by.css('#messagebox')).getText()).toBe('No matching items');
            })

            it('entering a valid search term should perform search and limit results', function(){
                browser.get('/');
                element(by.id('searchString')).sendKeys("val");

                browser.wait(function() {
                    return browser.isElementPresent(by.css('.search-results'));
                }, 5000);

                expect(element(by.css('.search-summary')).isPresent()).toBe(true);
                expect(element(by.css('.search-summary')).getText()).toBe('Showing 1 to 2 of 2 movies found');

                expect(hasClass(element(by.css('#messagebox')), 'ng-hide')).toBe(true);

                element.all(by.css('.thumbnail')).then(function(items) {
                    expect(items.length).toBe(2);;
                });
            })
        })

    });

});