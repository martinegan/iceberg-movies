var express = require('express');
var app = express();
module.exports = app;

var bodyParser = require('body-parser');

app.use(express.static(__dirname + '/../public'));
app.use('/bower_components', express.static(__dirname + '/../bower_components'));

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// set our port
var port = process.env.PORT || 8080;

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();

var sortByProperty = function(property) {
	'use strict';
	return function (a, b) {
		var sortStatus = 0;
		if (a[property] < b[property]) {
			sortStatus = -1;
		} else if (a[property] > b[property]) {
			sortStatus = 1;
		}

		return sortStatus;
	};
}

var filterResults = function(data, searchString) {
	var result = [];
	searchString = searchString != null ? searchString : '';
	searchString = searchString.toLowerCase();

	for(key in data) {
		var item = data[key];

		if(item.title.toLowerCase().indexOf(searchString) !== -1){
			result.push(item);
		}
	}

	return result;
}

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/movies', function(req, res) {
	var fs = require('fs');
	var obj;
	var response = {};

	fs.readFile(__dirname + '/data.json', 'utf8', function (err, data) {
		if (err) throw err;

		obj = JSON.parse(data);

		var searchOptions = {
			pageNumber: req.query.pageNumber != null ? req.query.pageNumber : 1,
			pageSize: req.query.pageSize != null ? req.query.pageSize : 20,
			start: req.query.start != null ? req.query.start : 1,
			end: req.query.end != null ? req.query.end : 20,
			sortBy: req.query.sortBy != null ? req.query.sortBy : 'title',
			searchTerm: req.query.searchTerm != null ? req.query.searchTerm : null
		};

		//set default variables
		var totalMovies, pageSize, currentPage, movieArrays, movieList, tempList;

		totalMovies = obj.movies.length;
		pageSize = searchOptions.pageSize;
		currentPage = 1;
		movieArrays = [];
		movieList = filterResults(obj.movies, searchOptions.searchTerm).sort(sortByProperty(searchOptions.sortBy));

		//split list into groups based on pageSize
		tempList = JSON.parse(JSON.stringify(movieList));

		while (tempList.length > 0) {
			movieArrays.push(tempList.splice(0, searchOptions.pageSize));
		}

		//set current page if specifed as get variable (eg: /?pageNumber=2)
		if (typeof searchOptions.pageNumber !== 'undefined') {
			currentPage = parseInt(searchOptions.pageNumber);
		}

		//show list of movies from group
		response.content = movieArrays[currentPage - 1] != null ? movieArrays[currentPage - 1] : [];
		response.pageNumber = currentPage;
		response.totalPages = movieArrays.length;
		response.matchedItems = movieList.length;
		response.totalItems = totalMovies;
		response.startItem = (parseInt(pageSize) * (currentPage-1))+1 != 0 ? (parseInt(pageSize) * (currentPage-1))+1 : 1;
		response.endItem = (parseInt(response.startItem) + parseInt(pageSize - 1)) <= response.matchedItems ? (parseInt(response.startItem) + parseInt(pageSize - 1)) : response.matchedItems;

		res.json(response);
	});
});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);